FROM nginx:1.23.2

LABEL Romain Chenard <rom.chenard@gmail.com>

ADD ./static-website-example /usr/share/nginx/html

RUN ls /usr/share/nginx/html

# RUN adduser myuser
# USER myuser


EXPOSE 80
# CMD gunicorn --bind 0.0.0.0:$PORT wsgi 
